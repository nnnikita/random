package com.random;

import com.random.services.Services;

public class Main {

    public static void main(String[] args) {

        Services services = new Services();

        services.outRandNum();
        services.out10RandNum();
        services.outTenRandNum0to10();
        services.outTenRandNum20to50();
        services.outTenRandNumMinus10to10();
        services.outRandCountRandNum();

    }
}
