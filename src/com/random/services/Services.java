package com.random.services;

public class Services {

    public void outRandNum() {
        System.out.println(Math.random());
    }

    public void out10RandNum() {
        for (int i = 0; i < 10; i++) {
            System.out.print(Math.random() + " ");
        }
        System.out.println();
    }

    public void outTenRandNum0to10() {
        int min = 0;
        int max = 10;
        for (int i = 0; i < 10; i++) {
            int rand = (int) ((Math.random() * (max + 1) - min)) + min;
            System.out.print(rand + " ");
        }
        System.out.println();
    }

    public void outTenRandNum20to50() {
        int min = 20;
        int max = 50;
        for (int i = 0; i < 10; i++) {
            int rand = (int) ((Math.random() * (max + 1) - min)) + min;
            System.out.print(rand + " ");
        }
        System.out.println();
    }

    public void outTenRandNumMinus10to10() {
        int min = -10;
        int max = 10;
        for (int i = 0; i < 10; i++) {
            int rand = (int) ((Math.random() * (max + 1) - min)) + min;
            System.out.print(rand + " ");
        }
        System.out.println();
    }

    public void outRandCountRandNum() {
        int min = -10;
        int max = 35;
        int minRange = 3;
        int maxRange = 15;
        int randCount = (int) ((Math.random() * (maxRange + 1) - minRange)) + minRange;
        for (int i = 0; i < randCount; i++) {
            int rand = (int) ((Math.random() * (max + 1) - min)) + min;
            System.out.print(rand + " ");
        }
        System.out.println();
    }

}
